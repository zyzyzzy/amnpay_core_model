<?php

namespace app\Payment\check;

use app\Payment\model\UserIp;

class CheckIp
{
    public function checkMain(&$return_json)
    {
        $ip = request()->ip();  //获取用户IP
        $pay_ip = UserIp::getPayIp($return_json['user_id']);
//        dump($pay_ip)
        if($pay_ip['pay_white_list_ip'] <> ''){  //如果白名单不为空，只有白名单里有的IP才能放行
            if(strstr($pay_ip['pay_white_list_ip'],$ip)){
                return true;
            }else{
                $return_json['msg'] = '您的IP['.$ip.']不在可用IP范围内';
                $return_json["ip"] = $ip;
                return false;
            }
        }else{
            if($pay_ip['pay_black_list_ip'] <> ''){
                if(strstr($pay_ip['pay_black_list_ip'],$ip)){
                    $return_json['msg'] = '您的IP['.$ip.']已被禁用';
                    $return_json["ip"] = $ip;
                    return false;
                }else{
                    return true;
                }
            }else{
                // 如果白名单、黑名单都没有设置，所有IP都放行
                return true;
            }
        }
    }
}
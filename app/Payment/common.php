<?php
/**
 * 编辑返回数据
 * @author 张杨 <zyzyzzy@vip.qq.com>
 * @return \think\response\Json
 */
function errorReturnJson($returnJson)
{
    $arrField = [
        'status'        => 'error',
        'log_msg'       => array_key_exists('msg',$returnJson)?$returnJson['msg']:'系统错误',
        'log_content'   => json_encode($returnJson),
        'ip'            => request()->ip(),
    ];
    array_key_exists('user_id', $returnJson)?$arrField['user_id']=$returnJson['user_id']:'';
    \app\Payment\model\PaymentLog::addLog($arrField);
    return json(['status' => 'error','mgs'=>array_key_exists('msg',$returnJson)?$returnJson['msg']:'系统错误']);
}

//获取ip
function get_ip()
{
    if (getenv("HTTP_CLIENT_IP") && strcasecmp(getenv("HTTP_CLIENT_IP"), "unknown")) {
        $ip = getenv("HTTP_CLIENT_IP");
    } else {
        if (getenv("HTTP_X_FORWARDED_FOR") && strcasecmp(getenv("HTTP_X_FORWARDED_FOR"), "unknown")) {
            $ip = getenv("HTTP_X_FORWARDED_FOR");
        } else {
            if (getenv("REMOTE_ADDR") && strcasecmp(getenv("REMOTE_ADDR"), "unknown")) {
                $ip = getenv("REMOTE_ADDR");
            } else {
                if (isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], "unknown")) {
                    $ip = $_SERVER['REMOTE_ADDR'];
                } else {
                    $ip = "unknown";
                }
            }
        }
    }
    return ($ip);
}

//判断是否为手机端访问
function is_mobile()
{
    // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset ($_SERVER['HTTP_X_WAP_PROFILE']))
        return true;

    //此条摘自TPM智能切换模板引擎，适合TPM开发
    if (isset ($_SERVER['HTTP_CLIENT']) && 'PhoneClient' == $_SERVER['HTTP_CLIENT'])
        return true;
    //如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset ($_SERVER['HTTP_VIA']))
        //找不到为flase,否则为true
        return stristr($_SERVER['HTTP_VIA'], 'wap') ? true : false;
    //判断手机发送的客户端标志,兼容性有待提高
    if (isset ($_SERVER['HTTP_USER_AGENT'])) {
        $clientkeywords = array(
            'nokia', 'sony', 'ericsson', 'mot', 'samsung', 'htc', 'sgh', 'lg', 'sharp', 'sie-', 'philips', 'panasonic', 'alcatel', 'lenovo', 'iphone', 'ipod', 'blackberry', 'meizu', 'android', 'netfront', 'symbian', 'ucweb', 'windowsce', 'palm', 'operamini', 'operamobi', 'openwave', 'nexusone', 'cldc', 'midp', 'wap', 'mobile'
        );
        //从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
            return true;
        }
    }
    //协议法，因为有可能不准确，放到最后判断
    if (isset ($_SERVER['HTTP_ACCEPT'])) {
        // 如果只支持wml并且不支持html那一定是移动设备
        // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
            return true;
        }
    }
    return false;
}

function wx_alipay_wap(){
    if (preg_match('~micromessenger~i', request()->header('user-agent'))) {
        return  'weixin';
    } else if (preg_match('~alipay~i', request()->header('user-agent'))) {
        return 'alipay';
    }else{
        return 'wap';
    }
}

/**
 * 支付通道数据传输加密
 *
 * @param $data
 * @return mixed
 */
function payment_sha1($data){
    $screkey_str = \think\facade\Config::get('payment.payment_md5_str');
    ksort($data);
    $sign_str = urldecode(http_build_query($data));
    $sign_str = substr($sign_str,0,-1);
    $sign = strtoupper(sha1($sign_str.$screkey_str));
    return $sign;
}

/**
 * 验证支付通道数据传输加密
 *
 * @param $data
 */
function verify_sha1($data){
    $sign = $data['sign'];
    unset($data['sign']);
    $check_sign = payment_sha1($data);
    if($check_sign == $sign){
        return true;
    }else{
        return false;
    }
}

/**
 * 判断是否为正整数
 * @param $str
 */
function is_num($str){
    if(preg_match("/^[1-9][0-9]*$/",$str)){
        return true;
    }else{
        return false;
    }
}

/**
 * 判断时间格式
 * @param $str
 * @return bool
 */
function is_datetime($str){
    if(preg_match("/^([12]\d\d\d)-(0?[1-9]|1[0-2])-(0?[1-9]|[12]\d|3[0-1]) ([0-1]\d|2[0-4]):([0-5]\d)(:[0-5]\d)?$/",$str)){
        return true;
    }else{
        return false;
    }
}
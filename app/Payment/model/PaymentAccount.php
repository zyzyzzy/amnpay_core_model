<?php
declare (strict_types = 1);

namespace app\Payment\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class PaymentAccount extends Model
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';
    // 设置字段信息
//    protected $schema = [
//
//    ];

}

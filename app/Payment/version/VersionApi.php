<?php
namespace app\Payment\version;

interface VersionApi
{

    /**
     * 检查收到的参数是否合法
     * @param $parameter 获取到的参数
     * @return mixed
     */
    function CheckParameter($parameter,&$msg_str);

    /**
     * 解密参数并返回待加密的字段数组
     * @param $parameter 获取的参数
     * @param $secretkey 密钥
     * @return bool
     */
    function DecryptData(&$parameter, $secretkey,&$msg_str);

    /**
     * 检测数据签名是否正确
     * @param $parameter 获取的参数
     * @param $signdata 验证原数据
     * @param $secretkey 密钥
     * @return bool
     */
    function CheckSign($parameter, $signdata, $secretkey,&$msg_str);

    /**
     * 标准格式化提交数据
     * @param $parameter
     */
    function FormatData($parameter,&$formatdata,&$msg_str);

    /**
     * 封装回调参数
     * @param $parameter
     * @return array
     */
//    function ReturnData($parameter,&$msg_str);

    /**
     * 回调数据加密
     * @param $signdata
     * @param $secretkey
     * @return bool|string
     */
    function GetSign($signdata, $secretkey,&$msg_str);

    /**
     * 异步回调方法
     */
    function notifyurl($parameter,&$msg_str);

    /**
     * 同步跳转回调
     */
    function callbackurl($parameter,&$msg_str);

    /**
     * 查询异步回调
     * @param $parameter
     */
    function queryNotifyurl($parameter,&$msg_str);

    /**
     * 查询响应输出
     * @param $parameter
     */
    function queryExit($parameter,&$msg_str);
}
<?php
return [
   'payment_check_class_field'=>[
        'CheckIp'       => 'normal',   //键为类名，值 normal 正式时执行，test 测试时判断  为空时都判断
    ],
    'version_lack_error_msg' => '缺少version参数',
    'version_path_error_msg' => 'version类文件不存在',
    'test_field' => 'amntest', //触发测试模的字段名，如果请求参数里有此参数触发测试模式
    'payment_md5_str' => '1234567890', //验证支付数据传输加密密钥
];
<?php
namespace app\Payment\model;

use think\Model;

class TradeList extends Model
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';
    // 设置字段信息
    protected $schema = [
        'id'                     => 'int', //主键
        'system_orderid'         => 'varchar', //我们系统生成的订单号
        'user_orderid'           => 'varchar',  //用户提交过来的订单号
        'trade_status'           => 'tinyint', //订单状态 0  未处理 1已付未返  2 已付已返
        'trade_type'             => 'tinyint', //订单类型 0  正常交易 1 测试
        'trade_apply_datetime'   => 'datetime', //交易申请时间
        'trade_success_datetime' => 'datetime',  //交易完成时间
        'create_time' => 'datetime',  //数据产生时间
        'update_time' => 'datetime',  //数据最后一次编辑时间
    ];

    // 设置字段类型转换
    protected $type = [
        'trade_status'    =>  'integer',
        'trade_type'     =>  'integer',
        'trade_success_datetime'  =>  'datetime',
    ];
    // 设置只读字段
    protected $readonly = ['system_orderid', 'user_orderid','trade_apply_datetime'];

    /**
     * 订单状态获取器
     * @param $value
     * @return mixed
     */
    public function getTradeStatusAttr($value)
    {
        $status = [0=>'未处理',1=>'成功未返',2=>'成功已返',3=>'作废',4=>'退款中',5=>'已退款'];
        return $status[$value];
    }

    /**
     * 订单类型获取器
     * @param $value
     * @return mixed
     */
    public function getTradeTypeAttr($value)
    {
        $type = [0=>'交易',1=>'测试'];
        return $type[$value];
    }
}
<?php
// +----------------------------------------------------------------------
// | Author: 张杨<zyzyzzy@vip.qq.com>
// +----------------------------------------------------------------------
// | 微信: zyzyzzy
// +----------------------------------------------------------------------
// | QQ：328662397
// +----------------------------------------------------------------------
// | Create_Date: 2020-10-13
// +----------------------------------------------------------------------

// 支付接口入口文件
declare(strict_types = 1);

namespace app\Payment\controller;


use app\Payment\model\PaymentVersion;
use think\Exception;
use think\facade\Config;

class Index
{
    public function index()
    {
        // 定义一个用来存储返回数据的变量
        $return_json = [];
        $return_json['parameter'] = request()->param(); //获取所有的请求变量（经过过滤）
        // 判断提交参数里是否有 version 参数
        if(!array_key_exists('version',$return_json['parameter'])){
            $return_json['msg'] = Config::get('payment.version_lack_error_msg');
            return errorReturnJson($return_json);
        }
        $return_json['mentod'] = strtolower(request()->method()); //获取请求类型 get,post
        if(is_mobile()){  //手机还是PC访问
            $return_json['pc_phone'] = 'phone';
            $return_json['wx_alipay_wap'] = wx_alipay_wap(); //微信还是支付宝访问
        }else{
            $return_json['pc_phone'] = 'pc';
            $return_json['wx_alipay_wap'] = ''; //微信还是支付宝访问
        }
        // 从数据库表里通过版本编号获取版本类名
        if(!PaymentVersion::getPaymentVersion($return_json)){
            return errorReturnJson($return_json);
        }
        $class_name =  "app\\Payment\\version\\".$return_json['version_classname'];
        if(!class_exists($class_name)){
            $return_json['msg'] = Config::get('payment.version_path_error_msg');
            return errorReturnJson($return_json);
        }
        $version_obj = new $class_name($return_json);
        // 获取要验证数据的类名数组
        $fun_arr = Config::get('payment.payment_check_class_field');
        // 遍历执行每个验证数据类
        foreach($fun_arr as $key => $val){
            if($val === 'normal' || $val === ''){
                if(preg_match("/[^A-Za-z]/",$key)){ // 如果类名包含除大小写英文字母以外的字符直接跳过
                    continue;
                }
                $class_name =  "app\\Payment\\check\\".$key;
                if(!class_exists($class_name)){ // 如果类文件不存在直接跳过
                    continue;
                }
                try {
                    $obj = new $class_name();  // 实例化数据验证类
                    if(!$obj->checkMain($return_json)){  //验证数据
                        return errorReturnJson($return_json);  //如果数据验证不通过 记录错误并返回错误
                    }
                }catch(Exception $exception){
                    echo($exception->getMessage());
                    continue;
                }
            }
        }

        if($version_obj->main()){
            // 如果自定义接口版本里的数据处里都正确后，下一步会把所有的数据提交给系统的核心通道处理类来处理。
            dump($return_json);
            $pay = new Payment($return_json);
            if(!$pay->index($return_json)){
                return errorReturnJson($return_json);
            }

        }else{
            return errorReturnJson($return_json);
        }
    }

    public function createOrderid()
    {
        /*$str = uniqid(strval(rand(1000,9999)),true);
        echo ($str);
        echo('<br>');
        echo(date("YmdHis"));
        echo('<br>');
        list($usec, $sec) = explode(" ", microtime());

        echo(((float)$usec + (float)$sec)*10000);
        echo('<br>');
        echo(rand(1000,9999));*/

        $orderid_leng = 32;
        $orderid_leng<16?$orderid_leng=16:'';
        $orderid_one = date('YmdHis');
        $orderid_two = '';
        if($orderid_leng >= 20){
            list($usec,$sec) = explode(" ",microtime());
            echo($usec);
            echo('<br>');
            echo($sec);
            echo('<br>');
            //$orderid_two = (floatval($usec) + floatval($sec))*10000;
            $orderid_two = substr(strval($usec),2,6) ;
            $exp = $orderid_leng - 20;
        }else{
            $orderid_two = "";
            $exp = $orderid_leng - 14;
        }
        $min = pow(10,$exp-1);
        $max = $min * 10 - 1;
        $orderid_three = rand($min,$max);
        echo($orderid_one);
        echo('<br>');
        echo($orderid_two);
        echo('<br>');
        echo($orderid_three);
        echo('<br>');
        echo($orderid_one.$orderid_two.$orderid_three);
    }
}

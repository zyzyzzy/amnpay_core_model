<?php
namespace app\Payment\model;

use think\Model;

class PaymentVersion extends Model
{
// 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';
    // 设置字段信息
    protected $schema = [
        'id'                        => 'int', //主键
        'numberstr'             => 'varchar', //版本号如 1.0.0
        'class_name'            => 'varchar',  //版本控制器名称
        'bieming'       => 'varchar', //别名
        'status'    => 'tinyint', //状态 1正常，0禁用
        'orderid_repeat' => 'smallint',  //用户订单号是否可以重复 0不可以，1可以
        'create_time'               => 'datetime',  //数据产生时间
        'update_time'               => 'datetime',  //数据最后一次编辑时间
    ];

    // 设置只读字段
    protected $readonly = ['numberstr', 'actionname'];

    // 在入口文件里通过接口版本编号获取类文件名和判断用户订单号是否可以重复的字段
    public static function getPaymentVersion(&$return_json)
    {
        $find = PaymentVersion::where('numberstr','=',$return_json['parameter']['version'])
                    ->field('class_name,orderid_repeat')
                    ->findOrEmpty();
        if($find->isEmpty()){
            $return_json['msg'] = 'version 参数错误';
            return false;
        }elseif (preg_match("/[^A-Za-z]/",$find['class_name'])){
            $return_json['msg'] = 'version类名格式错误';
            return false;
        }else {
            $return_json['orderid_repeat'] = $find['orderid_repeat'];//用户订单号是否可以重复 0不可以，1可以
            $return_json['version_classname'] = $find['class_name'];  //接口版本类文件名
            return true;
        }
    }

}
<?php
namespace app\Payment\model;

use think\Model;

class PaymentChannelMerchant extends Model
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';
    // 定义时间戳字段名
    protected $createTime = 'merchant_add_datetime';
    protected $updateTime = 'merchant_edit_datetime';
    //设置字段信息
    protected $schema = [
        'id' => 'int',
        'merchant_name' => 'varchar',
        'merchant_status' => 'tinyint',
        'merchant_add_datetime' => 'datetime',
        'merchant_edit_datetime' => 'datetime',
    ];
}
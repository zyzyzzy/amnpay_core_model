<?php
namespace app\Payment\model;


use think\Model;

class TradeAmount extends Model
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';
    // 设置字段信息
    protected $schema = [
        'id'                        => 'int', //主键
        'trade_list_id'             => 'int', //交易订单号ID
        'orderid_amount'            => 'decimal',  //订单金额(提交金额)
        'real_orderid_amount'       => 'decimal', //订单金额(实际支付金额)
        'charges_orderid_amount'    => 'decimal', //订单手续费
        'rate_orderid_amount'       => 'decimal', //订单手续费-费率
        'freeze_orderid_amount'     => 'decimal',  //冻结金额
        'charges_inside_qutside'    => 'tinyint',  //手续费内外扣，  0内扣，1外扣
        'create_time'               => 'datetime',  //数据产生时间
        'update_time'               => 'datetime',  //数据最后一次编辑时间
    ];

    // 设置字段类型转换
    protected $type = [
        'orderid_amount'            => 'float',  //订单金额(提交金额)
        'real_orderid_amount'       => 'float', //订单金额(实际支付金额)
        'charges_orderid_amount'    => 'float', //订单手续费
        'rate_orderid_amount'       => 'float', //订单手续费-费率
        'freeze_orderid_amount'     => 'float',  //冻结金额
    ];
    // 设置只读字段
    protected $readonly = ['trade_list_id', 'orderid_amount','real_orderid_amount','charges_orderid_amount','rate_orderid_amount','freeze_orderid_amount','charges_inside_qutside'];

    /**
     * 订单手续费内外扣获取器
     * @param $value
     * @return mixed
     */
    public function getChargesInsideQutsideAttr($value)
    {
        $arr = [0=>'内扣',1=>'外扣'];
        return $arr[$value];
    }

    public function getOrderidAmountAttr($value)
    {
        return sprintf("%.2f",$value);
    }

    public function getRealOrderidAmountAttr($value)
    {
        return sprintf("%.2f",$value);
    }

    public function getChargesOrderidAmountAttr($value)
    {
        return sprintf("%.2f",$value);
    }

    public function getFreezeOrderidAmountAttr($value)
    {
        return sprintf("%.2f",$value);
    }
}
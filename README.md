# 聚合支付系统核心模块 TP6 版

#### 介绍

我们开发了一套完整的开源的聚合支付系统 [易吉聚合支付系统](https://gitee.com/zyzyzzy/eger-aggregate-payment-system), 但是考虑到有很多小伙伴并不需要一个完整的聚合支付系统，只是想在自己已有的系统里添加一个聚合支付功能模块。基于这样的需求，我们把 [易吉聚合支付系统](https://gitee.com/zyzyzzy/eger-aggregate-payment-system) 里的核心模块（聚合支付模块）独立出来做成一个开源的小项目，并且用 ThinkPHP6 进行了重写，我们把所有聚合支付的功能做成一个应用模块，如果你的项目是用 TP6 开发的，可以直接复制这个应用文件夹到你的项目里使用。如果你的项目不是 TP6 开发的甚至不是 PHP 开发的也没有关系，我们会一步步详细的介绍具体的开发思路和逻辑，你按照我们提供的逻辑用自己熟悉的开发语言实现就可以了。

### 欢迎加入 QQ 群：869131049 共同交流探讨。

# [视频-从零开始写聚合支付系统](https://www.ixigua.com/6889091637356691980)



## <u>我写了十几年的代码，但是习惯没有养好，写注释和写文档不行，大家先看代码，注释和文档我再慢慢的来完善。喜欢的可以点个 Star 。</u>



# 更新日志

- ### 2020-10-27

  1. 完成了app\Payment\controller\PaymentAccount.php 里获取支付通道账号的逻辑代码，新增了数据库表
  
     具体的逻辑是：可以给用户指定某个通道分类的具体某个通道使用哪一个具体的通道账号，或者按照随时轮循或条件轮循的规律来获取通道账号。条件轮循分为 指定时间内可以使用这个账号，指定交易金额范围内可以使用这个账号，指定省市的用户可以用这个账号。  具体的大家看代码吧，有问题可以加QQ群：869131049 交流
  
- ### 2020-10-23

  1. 完善了app\Payment\controller\PaymentAccount.php 里获取支付通道账号的逻辑代码
  2. 在数据库里添加了支付通道当日交易总额的视图
  3. 这几天有点忙，有什么问题可以加QQ群：869131049 交流
  
- ### 2020-10-21

  1. 完善了app\Payment\controller\payment.php 里获取支付通道账号的逻辑代码
  2. 添加了数据库里支付通道账号相关的逻辑
  3. 凌晨三点半了，睡觉。明天接着弄。
  
- ### 2020-10-20

  1. 修改 app\Payment\version\VersionAbs.php 文件的 setVersionParameterValue、setVersionParameter、setVersionParameterSign方法
  2.  app\Payment\controller\payment.php 里添加了写入订单数据到数据库的代码
  3. 更新了数据库
  4. 今天到此，明天接着写，出去喝酒了
  
- ### 2020-10-19

  1. 接着完善了一下 app\Payment\controller\payment.php 代码. 明天接着写。具体的可以看看代码
  2. 修复了一些发现的小BUG
  3. 希望大家关注并加入QQ群：869131049，多提意见，只要不人身攻击，喷一下都可以。我会尽量每天都更新一点，最终一步步做成一个完整的聚合支付系统的开源项目。
  
- ### 2020-10-18

  1. 优化了入口文件的代码，添加了针对不同的接口版本设置用户订单号是否可以重复。分为两种情况，一种是同一用户的用户订单号不可以重复，还有一种是所有用户的用户订单号都不可以重复。

  2. 新增了数据库SQL文件

  3. 修改了验证支付通道数据传输加密方法(app\Payment\common.php **verify_sha1**)

  4. 新增了 app\Payment\controller\payment.php 代码, 还没有写完，明天接着写。

  5. ```php
     class Payment
     {
         public $parameter = "";
         public $payment_class_name = ""; //支付通道类名
         public $payment_obj = "";  //用来存储实例化的支付通道
         public function __construct(&$parameter)
         {
             $this->parameter = &$parameter;
         }
     
         public function index()
         {
             return (
                 $this->verifyData()
                 && $this->checkPaymentClass()
                 && $this->instantiatePaymentObj()
                 && $this->checkPaymentObj()
             );
         }
     
         //验证请求过来的数据是否合法
         private function verifyData()
         {
             if(!verify_sha1($this->parameter['formatdata'])){
                 $this->parameter['msg'] = '请求数据不合法';
                 return false;
             }
             return true;
         }
     
         //判断此用户是否可以调用所选通道分类
         private function checkPaymentClass()
         {
             //首先判断通道分类总开关是否半闭
             $PaymentChannelClassFind = PaymentChannelClass::where('classbm','=',$this->parameter['formatdata']['tongdao'])
                         ->field('id,status,classname')
                         ->findOrEmpty();
             if($PaymentChannelClassFind->isEmpty()){
                 $this->parameter['msg'] = '通道分类不存在';
                 return false;
             }
             if($PaymentChannelClassFind['status'] === 0){
                 $this->parameter['msg'] = '通道分类['.$this->parameter['formatdata']['tongdao'].']已禁用';
                 return false;
             }
     
             //接下来判断给此用户是否设置了此通分类下的具体支付通道（我太难了，写注释太痛苦了!!!!!）
             $UserPaymentChannelClassFind = UserPaymentChannelClass::where([
                 ['user_id','=',$this->parameter['formatdata']['usrid']],
                 ['payment_channel_class_id','=',$PaymentChannelClassFind['id']]
             ])->field('payment_channel_id')->findOrEmpty();
             if($UserPaymentChannelClassFind->isEmpty() || $UserPaymentChannelClassFind['payment_channel_id'] === 0){
                 $this->parameter['msg'] =  "用户在通道分类--".$PaymentChannelClassFind['classname']."--下无可用通道";
                 return false;
             }
     
             // 判断具体的支付通道
             $PaymentChannelFind = PaymentChannel::field('payment_codeing,payment_status')->findOrEmpty($UserPaymentChannelClassFind['payment_channel_id']);
             if($PaymentChannelFind->isEmpty()){
                 $this->parameter['msg'] = '支付通道不存在';
                 return false;
             }
             if($PaymentChannelFind['payment_status'] == 0){
                 $this->parameter['msg'] = '支付通道已禁用';
                 return false;
             }
             if (preg_match("/[^A-Za-z]/",$PaymentChannelFind['payment_codeing'])){
                 $return_json['msg'] = '支付通道文件名格式错误';
                 return false;
             }
             $this->payment_class_name = $PaymentChannelFind['payment_codeing'];
             return true;
         }
     
         // 实例化通道类
         private function instantiatePaymentObj()
         {
             $class_name =  "app\\Payment\\payment_channel\\".$this->payment_class_name;
             if(!class_exists($class_name)){
                 $return_json['msg'] = '支付通道类文件格不存在';
                 return false;
             }
             $this->payment_obj = new $class_name();
             return true;
         }
     
         //验证域名和状态
         private function checkPaymentObj()
         {
             //判断通道状态
             if(!(($this->payment_obj)->checkStatus)){
                 $this->parameter['msg'] = '支付通道类文件里已禁用';
                 return false;
             }
             //验证域名
             if(($this->payment_obj)->CheckDomain){
                 $user_domain = request()->host();
                 $count = Domain::where([
                     ['user_id','=',$this->parameter['formatdata']['userid']],
                     ['domain','=',$user_domain]
                 ])->count();
                 if($count <= 0){
                     $this->parameter['msg'] = '请求域名非法';
                     return false;
                 }
     
             }
             return true;
         }
     
     }
     ```

- ### 2020-10-17

  1. 修改了入口文件里的执行顺序。先实例化接口版本类，再遍历执行自定义数据验证类。
  2. 修改了接口版本类的抽像类文件 VersionAbs.php，在初始化函数里获取用户编号和用户ID。这样在入口文件里实例化接口版本类时，可以获取到当前用户的用户ID，以便在后面的数据验证类里可以针对具体的用户做事更加细致的验证。
  3. 完善了入口文件里的自定义数据验证类文件 CheckIp.php，添加了用户IP数据表 , 可以给每个用户设置支付IP白名单和支付IP黑名单。如果用户支付IP白名单非空，用户当前IP在白名单里才会放行。如果用户支付IP白名单为空黑名单非空，用户当前IP不在黑名单里才会放行。如果白名单和黑名单都为空，任何IP都放行。
  4. 修复了入口文件里数据验证类文件地址错误的问题。



#### 目录结构

├─app

│ ├─.htaccess

│ ├─AppService.php

│ ├─BaseController.php

│ ├─common.php

│ ├─event.php

│ ├─ExceptionHandle.php

│ ├─middleware.php

│ ├─Payment --------------------------- // 聚合支付应用模块

│ │ ├─check --------------------------- // 存储自定义数据验证类的文件夹

│ │ │ └─CheckIp.php ------------------- // 自定义数据验证类文件

│ │ ├─common.php

│ │ ├─config

│ │ │ └─payment.php ------------------- // 聚合支付应用模块配置文件

│ │ ├─controller

│ │ │ └─Index.php --------------------- // 支付接口入口文件

│ │ │ └─Payment.php --------------------- // 核心通道处理类

│ │ ├─payment_channel      //支付通道类文件夹

│ │ │ └─PaymentDemo.php --------------------- // 支付通道类文件

│ │ ├─event.php

│ │ ├─middleware.php

│ │ ├─model

│ │ │ ├─PaymentChannelMerchant.php

│ │ │ ├─PaymentLog.php

│ │ │ ├─PaymentVersion.php

│ │ │ ├─TradeAmount.php

│ │ │ ├─TradeList.php

│ │ │ ├─User.php

│ │ │ ├─UserIp.php

│ │ │ └─UserSecretkey.php

│ │ ├─version ------------------------- // 自定义版本文件夹

│ │ │ ├─DefaultVersion.php ------------ // 自定义版本类文件

│ │ │ ├─VersionAbs.php ---------------- // 抽象类，所有的自定义版本类必须 extends 它

│ │ │ └─VersionApi.php ---------------- // 接口类，所有的自定义版本类必须 implements 它

│ │ └─view

│ ├─provider.php

│ ├─Request.php

│ └─service.php

├─composer.json

├─composer.lock

├─config

├─extend

│ └─.gitignore

├─LICENSE

├─LICENSE.txt

├─public

├─README.md

├─route

│ └─app.php

├─think

├─vendor

└─view

└─README.md


<img src="http://www.aimanong.cn/869131049.jpg" style="zoom:30%;" />
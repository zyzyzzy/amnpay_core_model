<?php

namespace app\Payment\model;

use think\Model;

class UserSecretkey extends Model
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';
    // 设置字段信息
    protected $schema = [
        'id'                    => 'int', //主键
        'user_id'               => 'int', //我们系统生成的订单号
        'memberid'              => 'varchar',  //用户提交过来的订单号
        'md5str'                => 'varchar', //订单状态 0  未处理 1已付未返  2 已付已返
        'sys_publickey'         => 'text', //订单类型 0  正常交易 1 测试
        'sys_privatekey'        => 'text', //交易申请时间
        'user_publickey'        => 'text',  //交易完成时间
        'user_privatekey'       => 'text',  //交易完成时间
        'sys_publickey_path'    => 'varchar',  //交易完成时间
        'sys_privatekey_path'   => 'varchar',  //交易完成时间
        'create_time'           => 'datetime',  //数据产生时间
        'update_time'           => 'datetime',  //数据最后一次编辑时间
    ];

}
<?php
declare (strict_types = 1);

namespace app\Payment\controller;

use app\Payment\model\PaymentAccountKeystr;
use app\Payment\model\PaymentAccountLoops;
use app\Payment\model\PaymentAccountQuota;
use app\Payment\model\PaymentChannelAccount;
use app\Payment\model\UserPaymentAccount;
use app\Payment\model\UserPaymentChannelClass;
use app\Payment\model\PaymentAccount as PaymentAccountData;
use think\Request;

class PaymentAccount
{
    public $parameter = "";
    public $payment_account_list = [];
    public $payment_account_id = '';
    public function __construct(&$parameter)
    {
        $this->parameter = &$parameter;
    }

    public function index()
    {
        return $this->verifyData() && $this->getPaymentAccountList() && $this->getPaymentAccount();

    }

    //验证请求过来的数据是否合法
    private function verifyData()
    {
        if(!verify_sha1($this->parameter['formatdata'])){
            $this->parameter['msg'] = '请求数据不合法';
            return false;
        }
        return true;
    }

    //判断执行类型
    private function getPaymentAccountList()
    {
        $user_payment_account_count = UserPaymentAccount::where('user_payment_channel_class_id','=',$this->parameter['user_payment_channel_class_id'])->count();
        if($user_payment_account_count == 0){
            $default_payment_account_id = UserPaymentChannelClass::where([
                ['payment_channel_class_id','=',$this->parameter['payment_channel_class_id']],
                ['user_id','=',$this->parameter['user_id']],
                ['payment_channel_id','=',$this->parameter['payment_channel_id']]
            ])->value('defaultpayapiaccountid');
            // 如果设置了默认账号直接返回设置的账号
            if($default_payment_account_id){
                $this->payment_account_id = $default_payment_account_id;
            }else{  //如果没有设置默认账号，轮循此通道下的所有账号
                $this->payment_account_list = PaymentChannelAccount::where(['payment_channel_id','=',$this->parameter['payment_channel_id']])->select();
            }
        }else{ //如果给用户单独配置了通道账号，轮循单独配置的账号
            $this->payment_account_list = UserPaymentAccount::where('user_payment_channel_class_id','=',$this->parameter['user_payment_channel_class_id'])->select();
        }
        return true;

    }

    private function getPaymentAccount()
    {
        if($this->payment_account_id && !($this->checkPaymentAccountInfo($this->payment_account_id))){
            return false;
        }

        if($this->payment_account_list){
            foreach($this->payment_account_list as $key => $value){
                // 过滤掉不可用的账号
                if(!($this->checkPaymentAccountInfo($value['payment_account_id']))){
                    unset($this->payment_account_list[$key]);
                    continue;
                }
                $payment_account_loops_find = PaymentAccountLoops::where('payment_account_id','=',$$value['payment_account_id'])->findOrEmpty();
                // 如果此通道账号设置的是按条件轮循时，判断轮循条件
                if(!$payment_account_loops_find->isEmpty() && $payment_account_loops_find['status'] == 1){
                    // 判断是否在设置时间内
                    $user_time = time();
                    $datetime_ks = strtotime(date('Y-m-d').' '.$payment_account_loops_find['datetime_ks']);
                    $datetime_js = strtotime(date('Y-m-d').' '.$payment_account_loops_find['datetime_js']);
                    if($user_time > $datetime_ks || $user_time < $datetime_js){
                        unset($this->payment_account_list[$key]);
                        continue;
                    }
                    // 判断金额是否在设置范围内
                    $amount = floatval($this->parameter['formatdata']['amount'])/100;
                    if($amount < floatval($payment_account_loops_find['money_ks']) || $amount > floatval($payment_account_loops_find['money_js~'])){
                        unset($this->payment_account_list[$key]);
                        continue;
                    }

                    // 通过ip地址查询当前用户所在的省和市，然后来对比是否在所设置的省市范围内
                    // 这个需要自己去网上找一个这样的查询接口
                    $user_ip = request()->ip();  //用户IP
                    $user_province = ''; //用户所在省
                    $user_city = '';  //用户所在市
                    $province = $payment_account_loops_find['province'];
                    $city = $payment_account_loops_find['city'];
                    if(($province && $province != $user_province) || ($city && $city != $user_city)){
                        unset($this->payment_account_list[$key]);
                        continue;
                    }
                }
            }
            if(count($this->payment_account_list) == 0){
                $this->parameter['msg'] = "无可用的通道账号";
                return false;
            }else{
                $payment_account_id = $this->payment_account_list[array_rand($this->payment_account_list)]['payment_account_id'];
                $payment_account_find = PaymentAccountData::where('id','=',$payment_account_id)->findOrEmpty();
                $payment_account_keystr_find = PaymentAccountKeystr::where('payment_account_id','=',$payment_account_id)->findOrEmpty();
                if($payment_account_keystr_find->isEmpty()){
                    $this->parameter['msg'] = "通道账号没有配置密钥数据";
                    return false;
                }
                $this->parameter['payment_account_find'] = [
                    'payment_account_id' => $payment_account_id,
                    'memberid' => $payment_account_find['memberid'],
                    'account'  => $payment_account_find['account'],
                    'notifyurl' => $payment_account_find['notifyurl'],
                    'callbackurl' => $payment_account_find['callbackurl'],
                    'system_userid' => $payment_account_find['user_id'],
                    'md5_str' => $payment_account_keystr_find['md5keystr'],
                    'public_keystr' => $payment_account_keystr_find['publickeystr'], //配置的公钥
                    'private_keystr' => $payment_account_keystr_find['privatekeystr'], //配置的私钥
                    'upstream_keystr' => $payment_account_keystr_find['upstream_keystr'], // 上游配置给此通道账号的密钥
                    'keystr_password' => $payment_account_keystr_find['private_pwd']  // 上游给的密钥的密码
                ];
                // 如果开启了设置交易金额零头，按设置范围给交易金额加上一个零头
                if($payment_account_find['oddment'] == 1){
                    $this->parameter['odd_amount'] = intval($this->parameter['formatdata']['amount']) + intval(rand($payment_account_find['min_oddment'],$payment_account_find['max_oddment']));
                }else{
                    $this->parameter['odd_amount'] = $this->parameter['formatdata']['amount'];
                }
                return true;
            }

        }else{
            $this->parameter['msg'] = "无可用的通道账号";
            return false;
        }
    }

    //检查通道账号是否可用
    private function checkPaymentAccountInfo($payment_account_id)
    {
        $payment_account_find = PaymentAccountData::findOrEmpty($payment_account_id);
        if($payment_account_find->isEmpty()){
            $this->parameter['msg'] = "通道账号不存在";
            return false;
        }
        if($payment_account_find['status'] == 0){
            $this->parameter['msg'] = "通道账号已被禁用";
            return false;
        }
        $amount = floatval($this->parameter['formatdata']['amount'])/100; //订单金额
        if($amount < $payment_account_find['min_money'] || $amount > $payment_account_find['max_money']){
            $this->parameter['msg'] = "金额不能小于".$payment_account_find['min_money'].'或大于'.$payment_account_find['max_money'];
            return false;
        }
        // 获取此账号今天的已成功交易的金额
        $today_payment_account_amount = PaymentAccountQuota::whereTime('datetime','=',date('Y-m-d'))->where('id','=',$payment_account_id)->value('amount');
        $today_payment_account_amount?$today_payment_account_amount:$today_payment_account_amount=0;
        if($today_payment_account_amount > $payment_account_find['quota_money']){
            $this->parameter['msg'] = '当前账号今天交易限额已满';
            return false;
        }
    }
}

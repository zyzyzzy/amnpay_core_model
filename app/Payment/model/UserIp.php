<?php
declare (strict_types = 1);

namespace app\Payment\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class UserIp extends Model
{
    protected $autoWriteTimestamp = true;

    protected $schema = [
        'id'                     => 'int', //主键
        'user_id'                => 'int', //用户ID
        'memberid'               => 'varchar',  //用户编号
        'login_white_list_ip'    => 'varchar', //用户登录IP白名单
        'login_black_list_ip'    => 'varchar', //用户登录IP黑名单
        'pay_white_list_ip'      => 'varchar', //用户交易IP白名单
        'pay_black_list_ip'      => 'varchar',  //用户交易IP黑名单
        'create_time'            => 'datetime',  //数据产生时间
        'update_time'            => 'datetime',  //数据最后一次编辑时间
    ];

    //获取支付IP黑、白名单
    public static function getPayIp($user_id,$memberid='')
    {
        $pay_ip = UserIp::where('user_id','=',$user_id)->field('pay_white_list_ip,pay_black_list_ip')->findOrEmpty();
        if($pay_ip->isEmpty()){
           $pay_ip =  UserIp::create([
                'user_id' => $user_id,
                'memberid' => $memberid,
                'login_white_list_ip'    => '', //用户登录IP白名单
                'login_black_list_ip'    => '', //用户登录IP黑名单
                'pay_white_list_ip'      => '', //用户交易IP白名单
                'pay_black_list_ip'      => '',  //用户交易IP黑名单
            ]);
        }
        return ['pay_white_list_ip'=>$pay_ip->pay_white_list_ip,'pay_black_list_ip'=>$pay_ip->pay_black_list_ip];
    }
}

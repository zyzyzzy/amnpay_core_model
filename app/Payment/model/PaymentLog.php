<?php
namespace app\Payment\model;

use think\Model;

class PaymentLog extends  Model
{
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp = 'datetime';
    // 设置字段信息
    protected $schema = [
        'id'                        => 'int', //主键
        'log_msg'                   => 'varchar', //日志说明
        'log_content'               => 'text',  //日志详细内容
        'ip'                        => 'varchar', //IP地址
        'user_id'                   => 'int', //用户ID
        'create_time'               => 'datetime',  //数据产生时间
        'update_time'               => 'datetime',  //数据最后一次编辑时间
    ];
    protected $type = [
        'log_content'    =>  'array',
    ];

    // 设置只读字段
    protected $readonly = ['log_msg', 'ip','user_id','create_time'];

    public static function addLog(array $arrField)
    {
        return PaymentLog::create($arrField,['log_msg','log_content','ip','user_id']);
    }

}
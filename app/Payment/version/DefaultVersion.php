<?php

namespace app\Payment\version;
use app\Payment\version\VersionAbs;
use app\Payment\version\VersionApi;

class DefaultVersion extends VersionAbs implements VersionApi
{

    function SetMemberidOrderid()
    {
        $this->memberid_name = 'memberid';  //设置商户ID字段，用来获取用户的相关密钥信息
        $this->orderid_name = 'orderid';  //设置订单号字段，用来判断用户订单号是否重复
    }

    function setVersionParameter(&$parameter)
    {
        //版本所有参数 not_null不可能空， null可为空
        $parameter = [
            'orderid'       => "not_null",  //订单号
            'amount'        => "not_null",  //订单金额 单位分
            'memberid'      => "not_null", //商户编号
            'paytype'       => "not_null", //通道编码
            'orderdatetime' => "not_null", //订单提交时间 2020-02-02 20:20:20
            'signmethod'    => "not_null", //签名类型 md5 or rsa
            'notifyurl'     => "not_null", //异步回调地址
            'callbackurl'   => "not_null",  //同步跳转地址
            'bankcode'      => "null",  //银行编码
            'extend'        => "null",  //扩展字段
            'sign'          => "not_null"   //签名字段
        ];
    }


    function setVersionParameterSign(&$parameter)
    {
        //版本签名参数  not_null不可能空， null可为空
        $parameter = [
            'orderid'       => "not_null",  //订单号
            'amount'        => "not_null",  //订单金额 单位分
            'memberid'      => "not_null", //商户编号
            'paytype'       => "not_null", //通道编码
            'orderdatetime' => "not_null", //订单提交时间 2020-02-02 20:20:20
            'signmethod'    => "not_null", //签名类型 md5 or rsa
            'notifyurl'     => "not_null", //异步回调地址
            'callbackurl'   => "not_null",  //同步跳转地址
        ];

    }

    function CheckParameter($parameter, &$msg_str)
    {
        if (!preg_match('/[0-9a-zA-Z][^_]{6,32}/i', $parameter["orderid"])) {
            $msg_str = "订单号最短6个字符，最长32个字符，订单号只能由数字和大小写字母组成，不能包含 下划线";
            return false;
        }

        if(!is_num($parameter['amount'])){
            $msg_str = "amount 单位为分，必须是正整数";
            return false;
        }

        if(!is_datetime($parameter['orderdatetime'])){
            $msg_str = "orderdatetime 格式错误";
            return false;
        }

        if ($parameter["signmethod"] != "md5" and $parameter["signmethod"] != "rsa") {
            $msg_str = "签名方法只能是 md5 或 rsa";
            return false;
        }

        return true;
    }

    //  解密参数并返回待加密的字段数组
    function DecryptData(&$parameter, $secretkey, &$msg_str)
    {
       // 如果没有需要解密的数据直接返回
        return true;
    }

    function CheckSign($parameter, $signdata, $secretkey, &$msg_str)
    {
        if($parameter['signmethod'] == 'md5'){
            ksort($signdata);
            $sign_str = urldecode(http_build_query($signdata));
            $sign_str = substr($sign_str,0,-1);
            $sign = strtoupper(md5($sign_str.$secretkey['md5str']));
            if($sign != $parameter['sign']){
                $msg_str = "签名错误";
                return true;
            }else{
                return true;
            }
        }
    }

    function FormatData($parameter, &$formatdata, &$msg_str)
    {
        $formatdata = [
            'userid' => $this->user_id, //  用户ID
            'amount' => $parameter["amount"],  //交易金额 单位分
            'orderid' => $parameter["orderid"],  //用户订单号
            'callbackurl' => $parameter["callbackurl"],  //同步跳转回调地址
            'notifyurl' => $parameter["notifyurl"],   //异步回调地址
            'orderdatetime' => $parameter["orderdatetime"],    //交易订单时间
            'bankcode' => (array_key_exists('bankcode',$parameter))?($parameter["bankcode"]):"",   //银行编码
            'tongdao' => $parameter["paytype"],  //通道分类编码
            'extend' => (array_key_exists('extend',$parameter))?($parameter["extend"]):"",   //扩展字段
        ];
        return true;
    }

    function GetSign($signdata, $secretkey, &$msg_str)
    {
        // TODO: Implement GetSign() method.
    }

    function notifyurl($parameter, &$msg_str)
    {
        // TODO: Implement notifyurl() method.
    }

    function callbackurl($parameter, &$msg_str)
    {
        // TODO: Implement callbackurl() method.
    }

    function queryNotifyurl($parameter, &$msg_str)
    {
        // TODO: Implement queryNotifyurl() method.
    }

    function queryExit($parameter, &$msg_str)
    {
        // TODO: Implement queryExit() method.
    }
}
<?php
//支付通道示例类

namespace app\Payment\payment_channel;


class PaymentDemo
{
    public $PayName = "PaymentDemo";  //通道编码

    public $CheckDomain = true;  //是否判断提交域名

    public $CheckStatus = true;  //通道状态

    //生成系统订单号长度，不同的支付通道对应不同的上游，不同的上游可能对订单号的长度不同，所以这里可以单独的设置不同的支付通道的订单号长度
   // public $Sysordernumberleng = 32;

    public $tjurl = '';  //提交地址

    public function __construct()
    {

    }

}